Rails.application.routes.draw do

# Copyright (C) 2017 Heiko Bernloehr (FreeIT.de).
#
# This file is part of ECS.
#
# ECS is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# ECS is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with ECS. If not, see <http://www.gnu.org/licenses/>.

namespace :admin do
  resources :participants do
    post 'reset', on: :member
  end
  resources :communities do
    resources :participants
  end
  resources :organizations
  resources :ressources
end

scope :path => '/sys', :as => 'sys' do
  resources :memberships, :only => [:index]
  resources :events, :only => [:index] do
    match :fifo, :via => [:get, :post], :on => :collection
  end
  resources :configs, :only => [:index, :create]
  resources :subparticipants, :only => [:index, :show, :create, :update, :destroy]
end

begin
  Ressource.all.each do |r|
    scope :path => "/#{r.namespace}", :as => r.namespace  do
      resources r.ressource.to_sym, :controller => :messages, :only => [:index, :show, :create, :update, :destroy] do
        match :fifo, :via => [:get, :post], :on => :collection
        match :lifo, :via => [:get, :post], :on => :collection
        match :details, :via => [:get], :on => :collection
        match :details, :via => [:get], :on => :member
      end
    end
  end
rescue ActiveRecord::StatementInvalid
  Rails.logger.info "DB error: #{$!}"
end

# deprecated routes
resources :memberships, :only => [:index]
resources :events, :only => [:index] do
  match :fifo, :via => [:get, :post], :on => :collection
end

get '/admin' => 'admin/participants#index'
root 'admin/participants#default'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
