# Copyright (C) 2018 Heiko Bernloehr (FreeIT.de).
#
# This file is part of ECS.
#
# ECS is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# ECS is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with ECS. If not, see <http://www.gnu.org/licenses/>.

class Auth < ApplicationRecord
  belongs_to :message

  #scope :hash, lambda {|hash| {
  #  :joins => {:membership_messages => {:membership => :participant}},
  #  :order => "id ASC",
  #  :conditions => {:participants => {:id => participant.id}}}}


  # if valid time window return true
  def test_validation_window
    b = JSON.parse(message.body)
    sov = Time.parse(b["sov"])
    eov = Time.parse(b["eov"])
    if eov > Time.now
      true
    else
      false
    end
  end

  # garbage collect outtimed authorization tokens
  def self.gc_outtimed
    gc_sys_auths_lock= "#{Rails.root}/tmp/gc_sys_auths.lock"
    if File.exists?(gc_sys_auths_lock)
      logtext= "GC: there seems to be already running a ecs:gc_sys_auths process (#{gc_sys_auths_lock}). Aborting."
      logger.info logtext
      puts logtext unless Rails.env.test?
    else
      begin
        File.open(gc_sys_auths_lock,"w") do |f|
          f.puts "#{Process.pid}"
        end
        logtext= "GC: Searching for outtimed auths ..."
        logger.info logtext
        puts logtext unless Rails.env.test?
        Auth.all.each do |auth|
          if ! auth.test_validation_window
            auth.message.destroy_as_sender
            logtext= "GC: garbage collect auths token: #{auth.one_touch_hash}"
            logger.info logtext
            puts logtext unless Rails.env.test?
          end
        end
        logtext= "GC: Searching for outtimed auths done."
        logger.info logtext
        puts logtext unless Rails.env.test?
      ensure
        begin
          File.delete(gc_sys_auths_lock)
        rescue
        end
      end
    end
  end

end
