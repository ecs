# Copyright (C) 2016, 2017 Heiko Bernloehr (FreeIT.de).
#
# This file is part of ECS.
#
# ECS is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# ECS is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with ECS. If not, see <http://www.gnu.org/licenses/>.


class Admin::ParticipantsController < ApplicationController

  require 'pp'

  include Admin::Helper

  # TODO verify http methods

  def default
    redirect_to admin_participants_path
  end

  def index
    list
    render :action => 'list'
  end

  def list
    if params[:sys] == "true"
      @participants = Participant.all
    else
      @participants = Participant.all.where.not(name: "ecs")
    end

    @list_participants_anonymous_count = @participants.count - @participants.without_anonymous.count
    @participants = case params[:anonymous]
      when "true"
        @list_anonymous=true
        @list_participants_count = @participants.count
        @participants.distinct
      when "false"
        @list_anonymous=false
        @list_participants_count = @participants.count - @list_participants_anonymous_count
        @participants.without_anonymous.distinct
      else
        @list_anonymous=false
        @list_participants_count = @participants.count - @list_participants_anonymous_count
        @participants.without_anonymous.distinct
    end
    @action_buttons = case params[:actionbuttons]
                      when "true"
                        true
                      when "false"
                        false
                      else
                        false
                      end
  end

  def show
    @participant = Participant.find(params[:id])
  end

  def new
    @participant = Participant.new
    @organizations = Organization.all.order(:id)
    if ECS_CONFIG["participants"]["allow_anonymous"]
      @communities=Community.all.distinct
    else
      @communities=Community.all.where.not(name: "public").distinct
    end
    @participant.identities.build
  end

  def create
    @participant = Participant.new(participant_params)
    @participant.ptype = Participant::TYPE[:main]
    if @participant.save
      flash[:notice] = "Participant \"#{CGI.escapeHTML @participant.name}\" was successfully created."
      redirect_to admin_participants_path
    else
      @organizations = Organization.all.order(:id)
      render :action => 'new'
    end
  end

  def edit
    @participant = Participant.find(params[:id])
    @organizations = Organization.all.order(:id)
    if ECS_CONFIG["participants"]["allow_anonymous"]
      @communities=Community.all.distinct
    else
      @communities=Community.all.where.not(name: "public").distinct
    end
  end

  def reset
    @participant = Participant.find(params[:id])
    @participant.destroy_receiver_messages
    @participant.destroy_sender_messages
    @participant.destroy_events
    flash[:notice] = "Successfully cleared all sent and received messages and events of participant \"#{CGI.escapeHTML @participant.name}\"."
    redirect_to_admin_participants_path
  end

  def update
    params[:participant][:community_ids] ||= []
    @organizations = Organization.all.order(:id)
    @participant = Participant.find(params[:id])
    lmsgs= leaved_messages(@participant, params[:participant][:community_ids])
    if @participant.update(participant_params)
      generate_destroyed_events_by_leaving_a_community(@participant,lmsgs) unless lmsgs.blank?
      flash[:notice] = 'Participant was successfully updated.'
      redirect_to admin_participant_path(:id => @participant)
    else
      render :action => 'edit'
    end
  end

  def destroy
    p = Participant.find(params[:id])
    p.destroy
    flash[:notice] = "Participant \"#{CGI.escapeHTML p.name}\" was successfully destroyed."
    redirect_to_admin_participants_path
  end

  def index_communities
    @participant = Participant.find(params[:id])
    @communities=Participant.find(params[:id]).memberships.collect {|i| i.community  }.uniq.sort{|x,y| x.id <=> y.id }
  end

  # lists all those communities which the participant has not yet joined
  def index_noncommunities
    index_communities
    @communities=(Community.all - @communities).sort{|x,y| x.id <=> y.id }
  end

  def destroy_community
    destroy_membership(params[:c_id], params[:id])
    redirect_to admin_participant_communities_path(params[:id])
  end


  # join to a community
  def create_community
    create_membership(params[:c_id], params[:id])
    redirect_to admin_participant_communities_path(params[:id])
  end

private

  def redirect_to_admin_participants_path
    queryparams={}
    if params[:anonymous]
      queryparams[:anonymous]=true
    end
    if params[:actionbuttons]
      queryparams[:actionbuttons]=true
    end
    redirect_to admin_participants_path(queryparams)
  end

  # Generate destroyed events for all messages unconnected in respect to the
  # leaving communities.
  def generate_destroyed_events_by_leaving_a_community(participant, messages )
    messages.each do |msg|
      Event.make(:event_type_name => EvType.find_by_name("destroyed").name, :participant => participant, :message => msg)
      logger.info "destroyed event for message.id=#{msg.id}, participant:#{participant.name} (pid:#{participant.id})"
    end
  end

  def leaved_messages(participant, community_ids)
    leaved_community_ids= participant.communities.map{|c| c.id} - community_ids.map{|p| p.to_i}
    leaved_messages= []
    leaved_community_ids.each do |cid|
      leaved_messages << Membership.find_by_participant_id_and_community_id(participant.id, cid).messages
      leaved_messages << Community.find(cid).messages
    end
    leaved_messages.flatten.compact.uniq
  end

  def participant_params
    params.require(:participant).permit(:name, :description, :dns, :email, :organization_id, :community_selfrouting, :events_, community_ids: [],
                                        identities_attributes: [:id, :name, :description, :_destroy])
    #  Parameter-Example:
    #    { "utf8"=>"✓",
    #      "authenticity_token"=>"NQtz97vmdpbhtkRAwEaouDia55K+XXnApAN9+flu2sw=",
    #      "participant"=>
    #        {
    #          "name"=>"Universität Stuttgart ILIAS",
    #          "description"=>"Zentrale E-Learning Plattform.",
    #          "dns"=>"ilias3.uni-stuttgart.de",
    #          "email"=>"christian.bogen@tik.uni-stuttgart.de",
    #          "organization_id"=>"3",
    #          "community_selfrouting"=>"0",
    #          "events_"=>"1",
    #          "identities_attributes"=>
    #            {
    #              "0"=>
    #                {
    #                  "name"=>"27_vorkauf@rus.uni-stuttgart.de",
    #                  "description"=>"X.509 credentials",
    #                  "_destroy"=>"0",
    #                  "id"=>"4"
    #                },
    #              "1"=>
    #                {
    #                  "name"=>"A2_vorkauf@rus.uni-stuttgart.de",
    #                  "description"=>"X.509 credentials",
    #                  "_destroy"=>"0",
    #                  "id"=>"21"
    #                },
    #              "2"=>
    #                {
    #                  "name"=>"hubba bubba",
    #                  "description"=>"Und noch mehr"
    #                }
    #            },
    #            "community_ids"=>["1"]
    #        },
    #      "commit"=>"Save",
    #      "id"=>"12"
    #    }
  end

 
end
