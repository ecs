# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_02_28_115911) do

  create_table "auths", force: :cascade do |t|
    t.string "one_touch_hash"
    t.integer "message_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "communities", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "community_messages", force: :cascade do |t|
    t.integer "community_id"
    t.integer "message_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ev_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: :cascade do |t|
    t.integer "participant_id"
    t.integer "message_id"
    t.integer "ev_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "lock_version", default: 0
    t.index ["message_id"], name: "index_events_on_message_id"
    t.index ["participant_id"], name: "index_events_on_participant_id"
  end

  create_table "identities", force: :cascade do |t|
    t.integer "participant_id"
    t.string "name"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "membership_messages", force: :cascade do |t|
    t.integer "membership_id"
    t.integer "message_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "lock_version", default: 0
    t.index ["message_id"], name: "index_membership_messages_on_message_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.integer "participant_id"
    t.integer "community_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: :cascade do |t|
    t.string "content_type"
    t.integer "sender"
    t.text "body"
    t.integer "ressource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "removed", default: false
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "abrev"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "participants", force: :cascade do |t|
    t.string "name"
    t.string "dns"
    t.string "email"
    t.text "description"
    t.integer "organization_id"
    t.datetime "ttl"
    t.boolean "anonymous", default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "community_selfrouting", default: false
    t.boolean "events_", default: true
    t.string "ptype"
  end

  create_table "ressources", force: :cascade do |t|
    t.string "namespace"
    t.string "ressource"
    t.boolean "postroute", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "events", default: true
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["session_id"], name: "index_sessions_on_session_id"
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "subparticipants", force: :cascade do |t|
    t.integer "participant_id"
    t.integer "parent_id"
    t.string "realm"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
