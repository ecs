class AddIndexToMembershipMessages < ActiveRecord::Migration[4.2]
  def change
    add_index :membership_messages, :message_id, unique: false
  end
end
