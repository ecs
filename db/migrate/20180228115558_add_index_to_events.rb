class AddIndexToEvents < ActiveRecord::Migration[4.2]
  def change
    add_index :events, :participant_id, unique: false
    add_index :events, :message_id, unique: false
  end
end
